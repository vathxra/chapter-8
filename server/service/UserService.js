'use strict';


/**
 * Delete User Biodata
 *
 * id Integer  (optional)
 * returns inline_response_200
 **/
exports.deleteUser = function(id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : "data",
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get User Biodata
 *
 * id Integer  (optional)
 * returns inline_response_200_1
 **/
exports.getUser = function(id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "role" : "player",
    "updated_at" : "30-07-2021 12:00 +07:00",
    "level" : "novice",
    "nickname" : "fasya",
    "created_at" : "30-07-2021 12:00 +07:00",
    "id" : 1,
    "fullname" : "Vathiya Rezky Aliefasyah",
    "email" : "vathxra@gmail.com"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Update User Biodata
 *
 * body Object 
 * id Integer  (optional)
 * returns inline_response_200_1
 **/
exports.putUser = function(body,id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "role" : "player",
    "updated_at" : "30-07-2021 12:00 +07:00",
    "level" : "novice",
    "nickname" : "fasya",
    "created_at" : "30-07-2021 12:00 +07:00",
    "id" : 1,
    "fullname" : "Vathiya Rezky Aliefasyah",
    "email" : "vathxra@gmail.com"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

