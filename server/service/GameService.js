'use strict';


/**
 * Create Room
 *
 * body Object 
 * returns inline_response_201_2
 **/
exports.createRoom = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "room_id" : 1,
    "winner" : "Player 1",
    "round" : "ROUND 1",
    "updated_at" : "30-07-2021 12:00 +07:00",
    "name_room" : "Fasya Game",
    "created_at" : "30-07-2021 12:00 +07:00",
    "status" : "FINISHED"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get Room Information
 *
 * room_id Integer  (optional)
 * returns inline_response_201_2
 **/
exports.getRoom = function(room_id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "room_id" : 1,
    "winner" : "Player 1",
    "round" : "ROUND 1",
    "updated_at" : "30-07-2021 12:00 +07:00",
    "name_room" : "Fasya Game",
    "created_at" : "30-07-2021 12:00 +07:00",
    "status" : "FINISHED"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Room Fight
 *
 * body Object 
 * room_id Integer  (optional)
 * returns inline_response_201_2
 **/
exports.postFight = function(body,room_id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "room_id" : 1,
    "winner" : "Player 1",
    "round" : "ROUND 1",
    "updated_at" : "30-07-2021 12:00 +07:00",
    "name_room" : "Fasya Game",
    "created_at" : "30-07-2021 12:00 +07:00",
    "status" : "FINISHED"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

