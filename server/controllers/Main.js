'use strict';

var utils = require('../utils/writer.js');
var Main = require('../service/MainService');

module.exports.getRoot = function getRoot (req, res, next) {
  Main.getRoot()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
