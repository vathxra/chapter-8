'use strict';

var utils = require('../utils/writer.js');
var Game = require('../service/GameService');

module.exports.createRoom = function createRoom (req, res, next, body) {
  Game.createRoom(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getRoom = function getRoom (req, res, next, room_id) {
  Game.getRoom(room_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.postFight = function postFight (req, res, next, body, room_id) {
  Game.postFight(body, room_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
