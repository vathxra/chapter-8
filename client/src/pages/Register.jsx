import { useState, useEffect } from "react";
import { Form, Button } from "react-bootstrap";
import { useNavigate } from "react-router";
import Header from "../components/Header";

function App() {
  const initialValues = { username: "", password: "", confirmpw: "" };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmit, setIsSubmit] = useState(false);

  const navigate = useNavigate();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setFormErrors(validate(formValues));
    setIsSubmit(true);
    navigate("/login");
  };

  useEffect(() => {
    if (Object.keys(formErrors).length === 0 && isSubmit) {
      console.log(formValues);
    }
  }, [formErrors]);
  const validate = (values) => {
    const errors = {};
    if (!values.username) {
      errors.username = "Username is required!";
    } else if (!values.username.match(/^[0-9a-z]+$/)) {
      errors.username = "Username must be alphanumeric!";
    } else if (values.username.length < 6) {
      errors.username = "Username must be more than 6 characters";
    }
    if (!values.password) {
      errors.password = "Password is required!";
    } else if (values.password.length < 4) {
      errors.password = "Password must be more than 4 characters";
    } else if (values.password.length > 10) {
      errors.password = "Password cannot exceed more than 10 characters";
    }
    if (!values.confirmpw) {
      errors.confirmpw = "Confirm password is required!";
    } else if (values.confirmpw != values.password) {
      errors.confirmpw = "Password didn't match!";
    }
    return errors;
  };

  return (
    <>
    <Header />
    <div className="container">
      <Form onSubmit={handleSubmit}>
        <h1>REGISTER</h1>
        <Form.Group className="mb-3" controlId="formBasicName">
          <p>{formErrors.username}</p>
          <Form.Control
            type="text"
            name="username"
            value={formValues.username}
            onChange={handleChange}
            placeholder="Insert Your Username"
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <p>{formErrors.password}</p>
          <Form.Control
            type="password"
            name="password"
            placeholder="Password"
            value={formValues.password}
            onChange={handleChange}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <p>{formErrors.confirmpw}</p>
          <Form.Control
            type="password"
            name="confirmpw"
            placeholder="Confirm Password"
            value={formValues.confirmpw}
            onChange={handleChange}
          />
        </Form.Group>

        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form>
    </div>
    </>
  );
}

export default App;
