import { Form, Button } from "react-bootstrap";
import { useNavigate } from "react-router";
import { useState } from "react";
import { USERNAME, PASSWORD } from "./user";
import Header from "../components/Header";

function Login() {
    const [username, setUsername] = useState(null);
    const [password, setPassword] = useState(null);
    const [error, setError] = useState(null);


    const navigate = useNavigate();

    const handleLogin = (e) => {
        e.preventDefault();

        if (!username){
            setError("Username is required!")
            return;
        } if (!password) {
            setError("Password is required")
            return;
        }if (password !== PASSWORD || username !== USERNAME){
            setError("Username or Password is Wrong!")
            return;
        }
        
        localStorage.setItem("UNAME", JSON.stringify(username));
        localStorage.setItem("PASSWD", JSON.stringify(password));
        localStorage.setItem("LOGIN", JSON.stringify(true));
        navigate("/dashboard")

    }

  return (
      <>
      <Header />
        <div className="container">
            <Form onSubmit={handleLogin}>
                <Form.Group className="mb-3" controlId="formBasicUsername">
                <h1 className="title-form">LOGIN</h1>
                <Form.Control type="text" placeholder="Insert your name" onChange={(e) => setUsername(e.target.value)} />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Control type="password" placeholder="Insert your password" onChange={(e) => setPassword(e.target.value)} />
                </Form.Group>
                <Button variant="primary" type="submit">
                Login
                </Button>
                {!!error && <p>{error}</p>}
            </Form>
        </div>
    </>
  );
}

export default Login;
