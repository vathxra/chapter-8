import { useState } from "react";
import Opponent from "../components/Opponent";
import Header from "../components/Header";
import Users from "./opponents.json";
import { Card } from "react-bootstrap";

function Dashboard() {
  const [data, setData] = useState(Users);
  const [levelFilter, setlevelFilter] = useState(null);

  const level = ["NOVICE", "CLASS D", "CLASS C", "CLASS B", "CLASS A", "CANDIDATE MASTER", "GRAND MASTER"];

  return (
    <>
      <Header />
            <div>
              <div className="row d-flex justify-content-between">
                <h1 className="text-white py-5 header-text-title">Choose Your Opponent</h1>
              </div>
              {level.map((level) =>
                <Card className="text-white d-flex flex-row text-center align-items-center customCardLevel header-text-title cardLevelActive" id="cardLevel">
                  <Card.Body >{level}</Card.Body>
                </Card>
              )}
            </div>
     
        <div className="row pt-5 ">{levelFilter == null ? <Opponent Users={data} /> : 
        <Opponent Users={data.filter((p) => p.levels === levelFilter)} />}</div>

      
    </>
  );
};

export default Dashboard;