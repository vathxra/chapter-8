import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";

const Opponent = ({ Users }) => {
  return (
    <>
      {Users.map((player, index) => (
        <div className="col-3 p-3">
          <Card style={{ backgroundColor: "#262626", borderRadius: "10px", height: "300px" }} key={index} className="w-100 p-3">
            <Card.Header className="custom-header-footer-header">
              <div className="row">
                <div className="col-4">
                  <img src={player.avatar} alt={player.name} className="img-fluid rounded-circle" width="50" style={{ boxShadow: "7px 2px #FFB740" }} />
                </div>
                <div className="col-8">
                  <div style={{ fontWeight: "700", fontSize: "1rem", color: "#ffb548" }} className="header-text-title">
                    {player.name}
                  </div>
                  <div className="level-card-player">{player.level}</div>
                </div>
              </div>
            </Card.Header>
            <Card.Body className="text-white">
              <div className="bio-card-level">{player.bio}</div>
            </Card.Body>
            <Card.Footer className="pb-3 custom-footer-card">
              <div className="pb-2 custom-footer-card">Since {player.createdAt}</div>
              <Button className="rounded-pill w-50 bg-warning btn-card-footer " variant="warning">
                Fight
              </Button>
            </Card.Footer>
          </Card>
        </div>
      ))}
    </>
  );
};

export default Opponent;