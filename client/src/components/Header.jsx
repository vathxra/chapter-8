import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import Profile from "./Profile";
import { Link } from "react-router-dom";
import "../assets/css/style.css";

const Header = () => {
  const getLoginStatus = localStorage.getItem("LOGIN");
  const getUsername = localStorage.getItem("UNAME");

  return (
    <>
      <Navbar collapseOnSelect sticky="top" expand="lg" bg="dark" variant="dark"> 
          <Navbar.Brand className="header-text-title">Suit Games</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto"></Nav>
            {getLoginStatus === "true" ? (
              <NavDropdown title={getUsername.replace(/"/g, "")}>
                <Profile />
              </NavDropdown>
            ) : (
              <Nav>
                <Link to="/login" className="nav-link header-text-title">
                  Login
                </Link>
                <Link to="/register" className="nav-link header-text-title">
                  Register
                </Link>
              </Nav>
            )}
          </Navbar.Collapse>
      </Navbar>
    </>
  );
};

export default Header;